﻿Qian Ying Chen (M) - present
Harrison Ngo -present
Benjamin Staffan (U) -present


Meeting Minutes for September 20, 2016:

Completed last meeting: [1 minute]
        Unofficial first meeting.

Worked on: [5 minutes]
		Decide how players will get assigned their turn at the start. Players name, position and role 
		(colonel mustards, etc) will be determined at initialization. Role and position are randomized.
        
Goals for next meeting: [10 minutes]
        Discuss plans second user story.
        Determine how to test legal/illegal movements either by hard code or by JUnit tests.
        
 Schedule for next week: [1 minute]
        Second user story.