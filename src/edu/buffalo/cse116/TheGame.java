package edu.buffalo.cse116;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/*
 * Interfaces
 * @Author Alek, BEN
 * 
 */
public class TheGame extends JFrame
{	
	//Scanner sc = new Scanner(System.in);
	
	//int playerCount = sc.nextInt();
	static Die die = new Die();
	JButton rollButton = new JButton("Roll");
	JButton upButton = new JButton("Up");
	JButton downButton = new JButton("Down");
	JButton leftButton = new JButton("Left");
	JButton rightButton = new JButton("Right");
	JButton finalGuess = new JButton("Final");
	JButton check = new JButton("Check");
	SpringLayout layout = new SpringLayout();
	Board board = new Board();
	
	
	boolean isGameOver = false;
	Player winnerPlayer = null;

	
	public static void main(String[] args) {
		
		new TheGame();		
	}
	
	public TheGame() {
		//pls update
		pack(); 
        setTitle("Board Game");     
        setLayout(layout);
        setResizable(false);
        setLocation(100, 100);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(700, 550);
        setBackground(Color.WHITE);   
    	
    	rollButton.setBounds(530, 300, 155, 50);
    	rollButton.setPreferredSize(new Dimension(155, 50));
    	rollButton.setLocation(530, 300);
    	rollButton.setFocusPainted(false); 
        
    	check.setEnabled(false);
    	finalGuess.setEnabled(false);
        rollButton.setEnabled(true);
    	upButton.setEnabled(false);
    	downButton.setEnabled(false);
    	leftButton.setEnabled(false);
    	rightButton.setEnabled(false);      	
    	
    	check.setBounds(512+10, 250, 75, 25);
    	finalGuess.setBounds(512+100, 250, 75, 25);
    	upButton.setBounds(512+10, 360, 75, 25);
    	downButton.setBounds(512+100, 360, 75, 25);
    	leftButton.setBounds(512+10, 390, 75, 25);
    	rightButton.setBounds(512+100, 390, 75, 25);    
        
        rollButton.addActionListener(evt -> {
        	Players.getCurrentPlayer().setMoves(die.dieRoll());   
        	rollButton.setEnabled(false);
        	upButton.setEnabled(true);
        	downButton.setEnabled(true);
        	leftButton.setEnabled(true);
        	rightButton.setEnabled(true);
        	repaint();
        });   
        
        upButton.addActionListener(evt -> {movePlayer("up");});
        downButton.addActionListener(evt -> {movePlayer("down");});
        leftButton.addActionListener(evt -> {movePlayer("left");});
        rightButton.addActionListener(evt -> {movePlayer("right");});  
        finalGuess.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		makeAccusation();
        	}
        });
        
        check.addActionListener(new ActionListener() {
        

			@Override
			public void actionPerformed(ActionEvent e) {
				Players.getCurrentPlayer().makeSuggestion();
				
			}
        });
        
    	
        board.setPreferredSize(new Dimension(512,512));
        
    	addComponent(board, 0, 0, layout);
    	addComponent(check, 512+10, 250, layout);
    	addComponent(finalGuess,512+100, 250, layout);
        addComponent(rollButton, 530, 300, layout);
        addComponent(upButton, 512+10, 360, layout);
        addComponent(downButton, 512+100, 360, layout);
        addComponent(leftButton, 512+10, 390, layout);
        addComponent(rightButton, 512+100, 390, layout);    	

        setAlwaysOnTop(true);
    	setVisible(true);         
        
	}
	
	public void movePlayer(String position) {
		boolean execute = false;
		switch(position) {
		case "up":
			execute = Players.getCurrentPlayer().playerMove("up",16);
			break;
		case "down":
			execute = Players.getCurrentPlayer().playerMove("down",16);
			break;
		case "left":
			execute = Players.getCurrentPlayer().playerMove("left",16);
			break;
		case "right":
			execute = Players.getCurrentPlayer().playerMove("right",16);
			break;				
		}
		if(execute)
			Players.getCurrentPlayer().useMove();
		
		if(Players.getCurrentPlayer().getMoves() < 1) {
			rollButton.setEnabled(true);
        	upButton.setEnabled(false);
        	downButton.setEnabled(false);
        	leftButton.setEnabled(false);
        	rightButton.setEnabled(false);
        	Players.nextPlayer();
		}
		
		boolean enabled = false;
		for(Tile t : Board.getTiles()) {
			if(t.getX() == Players.getCurrentPlayer().getX() &&
			   t.getY() == Players.getCurrentPlayer().getY() &&
			   t.getTileType().equals(Tile.TileType.ROOM)) {
				enabled = true;
				break;
			}
			
		}		
		check.setEnabled(enabled);
    	finalGuess.setEnabled(enabled);		

		repaint();
		
		
	}
	public void makeAccusation(){
		String suspect = JOptionPane.showInputDialog("Enter suspect: ").toLowerCase();
		String room = JOptionPane.showInputDialog("Enter room: ").toLowerCase();
		String weapon = JOptionPane.showInputDialog("Enter weapon: ").toLowerCase();
		
		/*
		String wep = null;
		String person = null;
		String place = null;
		*/
		
		boolean accusationIsCorrect = true;
		
		for(Card card : Players.getMurderPile().getHand()) {
			String comparisonName = null;
			switch(card.getType()) {			
			case SUSPECT:
				comparisonName = suspect;
				break;
			case WEAPON:
				comparisonName = weapon;
				break;
			case ROOM:
				comparisonName = room;
				break;				
			}			
			if(!comparisonName.equals(card.getName().toLowerCase())) {
				accusationIsCorrect = false;
				break;
			}
		}
		if(accusationIsCorrect){
			System.out.println("The accusation was correct!");
			winnerPlayer = Players.getCurrentPlayer();			
			rollButton.setEnabled(false);
			upButton.setEnabled(false);
			downButton.setEnabled(false);
			leftButton.setEnabled(false);
			rightButton.setEnabled(false); 
			finalGuess.setEnabled(false);
			check.setEnabled(false);	
			isGameOver = true;	
			repaint();			
		}
			
		else{
			JOptionPane.showMessageDialog(null,Players.getCurrentPlayer().getName() + " made wrong accusation and is now out!");
			Players.playerOut();        	
        	if(Players.getPlayers().size() < 1) {
        		rollButton.setEnabled(false);
    			upButton.setEnabled(false);
    			downButton.setEnabled(false);
    			leftButton.setEnabled(false);
    			rightButton.setEnabled(false); 
    			finalGuess.setEnabled(false);
    			check.setEnabled(false);
        		isGameOver = true;
        	} else {   
        		rollButton.setEnabled(true);
            	upButton.setEnabled(false);
            	downButton.setEnabled(false);
            	leftButton.setEnabled(false);
            	rightButton.setEnabled(false);
        	Players.nextPlayer();
        	}			
			repaint();
		}
	}
	
	@Override
	public void paint(Graphics g) {
		 super.paint(g);
	     Graphics2D g2d = (Graphics2D) g;
	     if(!isGameOver) {
        g2d.setColor(Color.BLACK);
        g2d.drawString("Current Player: " + Players.getCurrentPlayer().getName(), 525, 50);        
        g2d.drawString("Cards: ", 525, 70);
        for(int i = 0; i < Players.getCurrentPlayer().getHand().size() ; i++) {
        	g2d.drawString(Players.getCurrentPlayer().getHand().get(i).getName() + " ("+
        			Players.getCurrentPlayer().getHand().get(i).getType().toString().charAt(0)+")", 525, 85+(i*15));
        }
        
        g2d.drawString("Moves remaining: " + Players.getCurrentPlayer().getMoves(), 525, 160);
        g2d.drawString("Card count: " + Players.getCurrentPlayer().getHand().size(), 525, 175);
        g2d.drawString("W = Weapon", 525, 200);
        g2d.drawString("S = Suspect", 525, 215);
        g2d.drawString("R = Room", 525, 230);
        
        
        g2d.setColor(Color.BLACK);
        g2d.fillRect(525, 470, 16, 16);
        g2d.drawString("= Wall", 550, 480);        
        
        g2d.setColor(Color.RED);
        g2d.fillRect(525, 490, 16, 16);
        g2d.setColor(Color.BLACK);
        g2d.drawString("= Room (with id)", 550, 500);
        
        g2d.setColor(Color.GREEN);
        g2d.fillRect(525, 510, 16, 16);
        g2d.setColor(Color.BLACK);
        g2d.drawString("= Player (with first letter)", 550, 520);
	     } else {
	    	 g2d.setColor(Color.BLACK);
	    	 g2d.fillRect(0, 0, 512, 512);
	    	 g2d.setColor(Color.WHITE);
	    	 if(winnerPlayer != null) {
	    	 g2d.drawString("Game Over! The winner is " + winnerPlayer.getName(), 32, 64);  
	    	 } else {
	    		 g2d.drawString("Game Over! There are no more players!", 32, 64);  
	    		 
	    	 }
	     }        
        Toolkit.getDefaultToolkit().sync();
        g.dispose();
	}
	
    private void addComponent(JComponent jComponent, int x, int y, SpringLayout layout) {
        add(jComponent);
        jComponent.setLocation(x, y);
        layout.putConstraint(SpringLayout.WEST, jComponent, x, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, jComponent, y, SpringLayout.NORTH, this);
    }


	
	
}
