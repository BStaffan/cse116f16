package edu.buffalo.cse116;
/*
 * @Author Harrison
 */
public class IllegalMoveException extends RuntimeException {
	public IllegalMoveException(){
		super("This is an illegal move");
	}
}
