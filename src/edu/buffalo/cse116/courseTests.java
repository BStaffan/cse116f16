package edu.buffalo.cse116;
/*
 * @Author Alek, Ben, Harrison, Qian Ying
 */
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

import edu.buffalo.cse116.Card.CardType;

public class courseTests {

	//No touchy 
	@Before 
	public void initialize() {
		Cards.initialize();
		Players.initialize();
	}
		
		/**
		 * Suggestion answered by next player tests (next 4 tests)
		 * 
		 * @author Qian Ying Chen
		 *
		 */
		@Test
		public void provePlayerCardTest01(){
			// suggestion would be answered by the next player because they have the player card
			int detective1 = 0; //player making the suggestion
			int detective2 = 1; //next player in the game
				
			Card suspectSuggested = new Card(CardType.SUSPECT, "Suspect");
			Card roomSuggested = new Card(CardType.ROOM, "Room");
			Card weaponSuggested = new Card(CardType.WEAPON, "Weapon");
			Player theSuggestion = new Player();
			theSuggestion.setCards(Arrays.asList(new Card[] { suspectSuggested, roomSuggested, weaponSuggested } ));
			System.out.println(roomSuggested);

			for (int i= 0; i < Players.getPlayers().size()-1; i++){
				if (i == detective2){
					if (Players.getPlayers().get(detective2).getHand().contains(suspectSuggested)){
							assertTrue(Players.getPlayers().get(i).hasMatchingAccusationCard(theSuggestion.getHand()));}
					else{ assertFalse(Players.getPlayers().get(i).hasMatchingAccusationCard(theSuggestion.getHand()));
					}
				}
			}
			
		}
		
		@Test
		public void proveRoomCardTest01(){
			// suggestion would be answered by the next player because they have the room card
			int detective1 = 0; //player making the suggestion
			int detective2 = 1; //next player in the game
				
			Card suspectSuggested = new Card(CardType.SUSPECT, "Suspect");
			Card roomSuggested = new Card(CardType.ROOM, "Room");
			Card weaponSuggested = new Card(CardType.WEAPON, "Weapon");
			Player theSuggestion = new Player();
			theSuggestion.setCards(Arrays.asList(new Card[] { suspectSuggested, roomSuggested, weaponSuggested } ));
			
			for (int i=0; i< Players.getPlayers().size()-1; i++){
				if (i == detective2){
					if (Players.getPlayers().get(i).getHand().contains(roomSuggested)){
							assertTrue(Players.getPlayers().get(i).hasMatchingAccusationCard(theSuggestion.getHand()));}
					else { assertFalse(Players.getPlayers().get(i).hasMatchingAccusationCard(theSuggestion.getHand()));
					}
				}
			}
			
		}
		
		@Test
		public void proveWeaponCardTest01(){
			// suggestion would be answered by the next player because they have the weapon card
			int detective1 = 0; //player making the suggestion
			int detective2 = 1; //next player in the game
				
			Card suspectSuggested = new Card(CardType.SUSPECT, "Suspect");
			Card roomSuggested = new Card(CardType.ROOM, "Room");
			Card weaponSuggested = new Card(CardType.WEAPON, "Weapon");
			Player theSuggestion = new Player();
			theSuggestion.setCards(Arrays.asList(new Card[] { suspectSuggested, roomSuggested, weaponSuggested } ));
			Player matchingWeaponCard = new Player();
			matchingWeaponCard.setCards(Players.getPlayers().get(detective2).getHand());
			
			for (int i=0; i< Players.getPlayers().size()-1; i++){
				if (i == detective2){
					if (Players.getPlayers().get(i).getHand().contains(weaponSuggested)){
							assertTrue(Players.getPlayers().get(i).hasMatchingAccusationCard(theSuggestion.getHand()));}
					else { assertFalse(Players.getPlayers().get(i).hasMatchingAccusationCard(theSuggestion.getHand()));
						
					}
				}
			}
		}
		
		@Test
		public void proveTwoMatchingCardTest01(){
			// suggestion would be answered by the next player because they have 2 matching cards
			int detective1 = 0; //player making the suggestion
			int detective2 = 1; //next player in the game
				
			Card suspectSuggested = new Card(CardType.SUSPECT, "Suspect");
			Card roomSuggested = new Card(CardType.ROOM, "Room");
			Card weaponSuggested = new Card(CardType.WEAPON, "Weapon");
			Player theSuggestion = new Player();
			theSuggestion.setCards(Arrays.asList(new Card[] { suspectSuggested, roomSuggested, weaponSuggested } ));
			Player twoMatchingCards = new Player();
			twoMatchingCards.setCards(Players.getPlayers().get(detective2).getHand());
			
			for (int i=0; i< Players.getPlayers().size()-1; i++){
				if (i == detective2){
						//what ? ?? 
				}
				else{
					assertFalse(Players.getPlayers().get(i).hasMatchingAccusationCard(theSuggestion.getHand()));
				}
			}
		}
	
	/*
	 * @Author Alek
	 */
	@Test
	public void alekTest1() {
		//Suggestion would be answered by the player after the next player because they have 1 or more matching cards;		
		
		int currentPlayer = 0;
		int nextPlayer = 1;
		int playerAfterNextPlayer = 2;
		
		if(playerAfterNextPlayer > Players.getPlayers().size())
			fail("We don't have enough players to test this method");
		
		Card oneOrMoreMatchingCard = Players.getPlayers().get(playerAfterNextPlayer).getHand().get(0);		
		Player theAccusation = new Player();
		
		theAccusation.setCards(Arrays.asList(new Card[] {
				oneOrMoreMatchingCard, 
				new Card(CardType.SUSPECT, "A"),
				new Card(CardType.ROOM, "B")
				}));
		
		for(int i = 0; i < Players.getPlayers().size()-1; i++) {
			if(i == playerAfterNextPlayer) {
				assertTrue(Players.getPlayers().get(i).hasMatchingAccusationCard(theAccusation.getHand()));				
			} else {
				assertFalse(Players.getPlayers().get(i).hasMatchingAccusationCard(theAccusation.getHand()));
			}
		}
		
		//assertTrue(Players.getPlayers().get(playerAfterNextPlayer).hasMatchingAccusationCard(theAccusation.getHand()));			
	}
	/*
	 * @Author Alek
	 */
	@Test
	public void alekTest2() {
		//Suggestion would be answered by the player immediately before player making suggestion
		//because they have 1 or more matching cards;	
		
		int playerBeforePlayerMakingSuggestion = 0;
		int playerMakingSuggestion = 1;		
	
		Card oneOrMoreMatchingCard = Players.getPlayers().get(playerBeforePlayerMakingSuggestion).getHand().get(0);			
		Player theAccusation = new Player();		
		theAccusation.setCards(Arrays.asList(new Card[] {
				oneOrMoreMatchingCard, 
				new Card(CardType.SUSPECT, "A"),
				new Card(CardType.ROOM, "B")
				}));	
		
		for(int i = 0; i < Players.getPlayers().size()-1; i++) {
			if(i == playerBeforePlayerMakingSuggestion) {
				assertTrue(Players.getPlayers().get(i).hasMatchingAccusationCard(theAccusation.getHand()));				
			} else {
				assertFalse(Players.getPlayers().get(i).hasMatchingAccusationCard(theAccusation.getHand()));
			}
		}
	}
	/*
	 * @Author Alek
	 */
	@Test
	public void alekTest3() {
		//Suggestion cannot be answered by any player but the player making the suggestion 
		//has 1 or more matching cards;
		
		//Chose a random player
		int playerMakingSuggestion = 3;
	
		Card oneOrMoreMatchingCard = Players.getPlayers().get(playerMakingSuggestion).getHand().get(0);			
		Player theAccusation = new Player();		
		theAccusation.setCards(Arrays.asList(new Card[] {
				oneOrMoreMatchingCard, 
				new Card(CardType.SUSPECT, "A"),
				new Card(CardType.ROOM, "B")
				}));	
		
		for(int i = 0; i < Players.getPlayers().size()-1; i++) {
			if(i == playerMakingSuggestion) {
				assertTrue(Players.getPlayers().get(i).hasMatchingAccusationCard(theAccusation.getHand()));				
			} else {
				assertFalse(Players.getPlayers().get(i).hasMatchingAccusationCard(theAccusation.getHand()));
			}
		}
	}
	/*
	 * @Author Alek
	 */
	@Test
	public void alekTest4() {
		//Suggestion cannot be answered by any player and the player making 
		//the suggestion does not have any matching cards.

		Player theAccusation = new Player();		
		theAccusation.setCards(Arrays.asList(new Card[] {
				new Card(CardType.WEAPON, "A"),
				new Card(CardType.SUSPECT, "B"),
				new Card(CardType.ROOM, "C")
				}));	
		
		for(int i = 0; i < Players.getPlayers().size()-1; i++) {
			assertFalse(Players.getPlayers().get(i).hasMatchingAccusationCard(theAccusation.getHand()));			
		}
	}
	
	/*
	 * @author Ben
	 */
	@Deprecated
	@Test
	public void legalHorizWTest()
	{
		/*
		int dieRoll;
		DisBoard board = new DisBoard();
		Player hertz = new Player();
		hertz.setX(15);
		hertz.setY(7);
		int orgX=hertz.getX();
		Die die = new Die();
		dieRoll = die.dieRoll();

		hertz.playerMove("left",dieRoll);
		assertEquals(hertz.getX(),orgX-dieRoll,0.0);
		*/
	}
	/*
	 * @author Ben
	 */
	@Deprecated
	@Test
	public void legalHorizETest()
	{
		
		/*
		int dieRoll;
		DisBoard board = new DisBoard();
		Player hertz = new Player();
		Die die = new Die();
		dieRoll = die.dieRoll();
		hertz.playerMove("right",dieRoll);
		assertEquals(hertz.getX(),dieRoll,0.0);
		*/
	}
	/*
	 * @author Ben
	 */

	@Test
	public void legalVertTest() {
		Player hertz = new Player();
		int dieRoll=0;
		hertz.setX(5);
		hertz.setY(10);
		Die die = new Die();
		dieRoll = die.dieRoll();
		int orgY=hertz.getY();
		hertz.playerMove("up",dieRoll);
		assertEquals(hertz.getY(),orgY-dieRoll,0.0);		
	}
	/*
	 * @author Ben
	 */
	@Test
	public void legalVertSTest() {
		Player hertz = new Player();
		int dieRoll=0;
		Die die = new Die();
		dieRoll = die.dieRoll();
		hertz.playerMove("down",dieRoll);
		assertEquals(hertz.getY(),dieRoll,0.0);		
	}
	
	/*
	 * Testing for being able to move at split positions, moves on the y axis first then x.
	 * Goes in sequential testing to also make sure moving between turns works
	 * 
	 * @author Ben
	 */
	@Test
	public void legalHorizAndVertTest() 
	{
		int testTwoX = 0;
		int testTwoY = 0;
		int testThreeX = 0;
		int testThreeY = 0;
		int testFourX = 0;
		int testFourY = 0;
		Player hertz = new Player("Hertz",null,10,10);
		int dieRoll=0;
		Die die = new Die();
		dieRoll = die.dieRoll();
		//hertz.playerMove("downRight",dieRoll,2);
		assertEquals(hertz.getX(),(10 + dieRoll-2),0.0);
		testTwoX = hertz.getX();
		assertEquals(hertz.getY(),12,0.0);
		testTwoY = hertz.getY();
		//Test for moving down and to the left
		dieRoll = die.dieRoll();
		//hertz.playerMove("downLeft",dieRoll,2);
		assertEquals(hertz.getX(),(testTwoX - (dieRoll-2)),0.0);
		testThreeX = hertz.getX();
		assertEquals(hertz.getY(),(testTwoY +2),0.0);
		testThreeY = hertz.getY();
		//Test for moving up to left
		dieRoll = die.dieRoll();
		//hertz.playerMove("upLeft",dieRoll,2);
		assertEquals(hertz.getX(),(testThreeX - (dieRoll-2)),0.0);
		testFourX = hertz.getX();
		assertEquals(hertz.getY(),(testThreeY -2),0.0);
		testFourY = hertz.getY();
		//Test for moving up to right
		dieRoll = die.dieRoll();
		//hertz.playerMove("upRight",dieRoll,2);
		assertEquals(hertz.getX(),(testFourX + (dieRoll-2)),0.0);
		testThreeX = hertz.getX();
		assertEquals(hertz.getY(),(testFourY -2),0.0);
		testThreeY = hertz.getY();
	}
	/*
	 * test is checking for being able to enter room at this spot if not located at door 
	 * cannot call the open door function
	 * @author Ben
	 */
	@Test
	public void legalDoorTest() 
	{
		Player hertz = new Player("Hertz",null,10,3);
		//char starts at space 10,3
		hertz.playerMove("left",5);
		//moves 5 spots and lands at door
		assertEquals(hertz.getX(),5,0.0);
		assertEquals(hertz.getY(),3,0.0);
		// enters room
		hertz.openDoor();
		assertEquals(hertz.getX(),3,0.0);
		assertEquals(hertz.getY(),3,0.0);
	}
	
	/*
	 * same premise as open door, will not allow in passage unless in room
	 * @author Ben
	 */
	@Test
	public void legalPassageTest() {
		Player hertz = new Player("Hertz",null,10,3);
		//char starts at space 10,3
		hertz.playerMove("left",5);
		//moves 5 spots and lands at door
		assertEquals(hertz.getX(),5,0.0);
		assertEquals(hertz.getY(),3,0.0);
		// enters room
		hertz.openDoor();
		assertEquals(hertz.getX(),3,0.0);
		assertEquals(hertz.getY(),3,0.0);
		//chooses yes to enter secret passage
		//hertz.passageMove(true);
		assertEquals(hertz.getX(),25,0.0);
		assertEquals(hertz.getY(),25,0.0);
		
	}
	/*
	 * @Author Harrison
	 */
	@Deprecated
	@Test
	public void illegalMoveMoreThanDieTest(){
		/*
		DisBoard board=new DisBoard();
		Player hertz6= new Player();

		hertz6.setX(5);
		hertz6.setY(5);
		Die die=new Die();
		int dieroll=die.dieRoll();
		hertz6.playerMove("left", dieroll);
		assertFalse(hertz6.getX()+1==hertz6.getX());

		int dieroll2=die.dieRoll();
		Player hertz1=new Player();
		hertz1.playerMove("down", dieroll2);
		assertFalse(dieroll2+1==hertz1.getY());

		Player hertz2=new Player();
		DisBoard board2=new DisBoard();
		board2.board[5][5]=hertz2;
		hertz2.setX(5);
		hertz2.setY(5);
		int dieroll3=die.dieRoll();
		int right=(dieroll3==1)?0:dieroll3-1;
		board2.board[hertz2.getY()-1][hertz2.getX()+right]=hertz2;
		hertz2.playerMove("up",1);
		hertz2.playerMove("right",right);
		assertEquals(hertz2,board2.board[hertz2.getY()][hertz2.getX()]);
		//over here I checked to see what was at the board if you move two spots up
		assertFalse(board2.board[hertz2.getY()-1][hertz2.getX()]==hertz2);

*/


	}
	/*
	 * @Author Harrison
	 */
	@Deprecated
	@Test
	public void illegalMoveDiagonalTest(){
		/*
		Die die=new Die();
		Player hertz3=new Player();
		DisBoard board1=new DisBoard();
		board1.board[5][5]=hertz3;
		hertz3.setX(5);
		hertz3.setY(5);
		int dieroll=die.dieRoll();

		hertz3.playerMove("down", dieroll);
		assertFalse(6==hertz3.getX());
		*/
	}
	/*
	 * @Author Harrison
	 */
	@Deprecated
	@Test
	public void illegalMovNotContiguousTest(){
		/*
		DisBoard board3=new DisBoard();
		Die die=new Die();
		Player hertz4=new Player();
		int dieroll=die.dieRoll();
		//assume player wants to move 5 to the right 
		hertz4.setX(5);
		hertz4.setY(5);
		hertz4.playerMove("right", dieroll);
		//player should not end up a n+1 distance away to the right when he rolled n and wanted to move strictly right
		board3.board[5][hertz4.getX()+dieroll]=hertz4;
		//this shows that the n+1 distance away is not where the player is
		assertFalse(board3.board[5][hertz4.getX()+dieroll+1]==hertz4);
		//this shows that the player is in the correct spot
		assertEquals(hertz4,board3.board[5][hertz4.getX()+dieroll]);
		*/
		
		
	}
	/*
	 * @Author Harrison
	 */
	@Deprecated
	@Test(expected=IllegalMoveException.class)
	public void illegalMoveGoesThroughWallTest(){
		/*
		DisBoard board2=new DisBoard();
		Die die=new Die();
		Player hertz5=new Player();
		int dieroll=die.dieRoll();
		//Let's say the player wants to move left when he is already on the left side and the wall is the next spot over
		hertz5.setX(0);
		hertz5.setY(5);
		hertz5.playerMove("left", dieroll);
		//Let's say the player wants to move down when he is already on the bottom side and the wall is "under"
		hertz5.setX(7);
		hertz5.setX(25);
		hertz5.playerMove("down", dieroll);
		*/
	}

}