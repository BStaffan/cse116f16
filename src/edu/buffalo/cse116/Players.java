package edu.buffalo.cse116;

import java.util.Scanner;

/*
 * @author Alek , Ben
 */
public class Players {
	
	static java.util.List<Player> players =  new java.util.ArrayList<Player>();
	static String[] playerNames = {"Scarlet", "Plum", "Green","White","Peacock","Col Must"};
	static java.util.List<Player> naughtyList =  new java.util.ArrayList<Player>();
	static int currentPlayer = 0;
	static int playerCount = 0;
	static Player murderer;
	public static void initialize() {
		Scanner sc = new Scanner(System.in);
		System.out.println("How Many Players?");
		playerCount = sc.nextInt();
		
		murderer= new Player("Murderer",Cards.deal3Cards(),0,0);
		System.out.println("The murderer has the following cards: ");
		for(Card card : murderer.getHand()) {			
			System.out.println("-CardType: " + card.getType().name() +", " + card.getName());
		}
		
		for(int i = 0; i < playerCount; i++) {
			players.add(new Player(playerNames[i], Cards.deal1Cards(), 16 + (i*32), 16));
			
		}
		
		while(Cards.deckEmpty()!=true){
			for(int i = 0; i < playerCount; i++) {
				if((Cards.deckEmpty()!=true)){
				players.get(i).addToHand(Cards.handDeal());
				}
			}
		}
		/*
		for(Player player : players) {
			System.out.println(player.getName() + " has the following <"+ player.getHand().size()+"> cards: ");
			for(Card card : player.getHand()) {
				System.out.println("-CardType: " + card.getType().name() +", " + card.getName());
			}
		}
		*/
	}
	
	public static java.util.List<Player> getPlayers() {
		return players;
	}	
	
	public static Player getCurrentPlayer() {
		if(players.get(currentPlayer).getStatus()){
		return players.get(currentPlayer);
		}
		else{
		return players.get(currentPlayer+1);
		}
			
	}
	public static Player getMurderPile() {
		return murderer;
	}
	public static Player getNextPlayer(){
		if(players.get(currentPlayer+1)!=null){
			currentPlayer+=1;
			return players.get(currentPlayer+1);
		}
		else{
			currentPlayer=0;
			return players.get(0);
	}
	}
	
	public static void nextPlayer() {
		if(currentPlayer + 1 > players.size()-1)
			currentPlayer = 0;
		else
			currentPlayer++;
	}
	public static void playerOut(){
		naughtyList.add(players.get(currentPlayer));
		players.get(currentPlayer).isOut(false);
		
	}

}
