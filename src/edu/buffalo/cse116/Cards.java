package edu.buffalo.cse116;

import java.util.Collections;

import edu.buffalo.cse116.Card.CardType;
/*
 * @author Alek,Ben 
 */
public class Cards 
{	
	
	static java.util.List<Card> weaponCards = new java.util.ArrayList<Card>();
	static java.util.List<Card> suspectCards = new java.util.ArrayList<Card>();
	static java.util.List<Card> roomCards = new java.util.ArrayList<Card>();
	static java.util.List<Card> deck = new java.util.ArrayList<Card>();
	
	static String[] weapons = {"Lead Pipe", "Revolver", "Knife", "Rope", "Candle Stick", "Wrench"};
	static String[] suspects = {"Professor Plum", "Mrs. White", "Mr. Green", "Mrs. Peacock", "Miss. Scarlett", "Colonel Mustard"};
	static String[] rooms = {"Study", "Library", "Conservatory", "Hall", "Kitchen", "Ballroom", "Dining Room", "Lounge", "Billiard Room"};
	/**initializes the 4 decks. 1 group deck and 3 decks broken down by types
	 *
	 * @param  None
	 * @return      Cards from deck to players hand
	 * @author Alek & Ben
	 */
	public static void initialize() {
		for(String weapon : weapons){
			weaponCards.add(new Card(CardType.WEAPON, weapon));
			deck.add(new Card(CardType.WEAPON, weapon));
		}
		for(String suspect : suspects){
			suspectCards.add(new Card(CardType.SUSPECT, suspect));
			deck.add(new Card(CardType.SUSPECT, suspect));
		}
		for(String room : rooms){
			roomCards.add(new Card(CardType.ROOM, room));
			deck.add(new Card(CardType.ROOM, room));
		}
		//System.out.println("Cards added to deck: " + (weaponCards.size() + suspectCards.size() + roomCards.size()));		
	
	}
	/**Returns 3 cards, can be used for the guessing pile and also when 6 people play
	 *
	 * @param  None
	 * @return None
	 * @author Alek & Ben
	 */
	public static java.util.List<Card> deal3Cards() {
		java.util.List<Card> dealCards = new java.util.ArrayList<>();
		Collections.shuffle(weaponCards);
		Collections.shuffle(suspectCards);
		Collections.shuffle(roomCards);
		
		Card weaponCard = weaponCards.get(0);
		Card suspectCard = suspectCards.get(0);
		Card roomCard = roomCards.get(0); 
		
		dealCards.add(weaponCard);

		System.out.println();
		deck.remove(weaponCard);
		weaponCards.remove(weaponCard);
		
		dealCards.add(suspectCard);		
		deck.remove(suspectCard);
		suspectCards.remove(suspectCard);
		
		dealCards.add(roomCard);		
		deck.remove(roomCard);
		roomCards.remove(roomCard);		

		return dealCards;
		
	}
	public static java.util.List<Card> deal1Cards() {
		java.util.List<Card> dealCards = new java.util.ArrayList<>();
		dealCards.add(deck.get(0));
		deck.remove(0);
		return dealCards;
		
	}
	/**Checks to see if their are still cards to deal
	 *
	 * @param  None
	 * @return Boolean true or false
	 * @author BenStaffan
	 */
	public static boolean deckEmpty(){
		return deck.isEmpty();
	}
	
	/**Returns cards to player until deck is empty
	 *
	 * @param  Takes number of players to determine hand size
	 * @return Cards from deck to players hand
	 * @author BenStaffan
	 */
	public static Card handDeal() {
		//deck assembly
		Card dealCards = null;
		Collections.shuffle(deck);
		dealCards = deck.get(0);
		deck.remove(0);
		return dealCards;
	}
}
