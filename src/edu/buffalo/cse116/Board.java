package edu.buffalo.cse116;

import java.awt.Color;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;


import javax.swing.JPanel;


import edu.buffalo.cse116.Tile.TileType;

public class Board extends JPanel {
	
	static int DimensionX = 512;
	static int DimensionY = 512;
	static int TileDimension = 16;
	static java.util.List<Tile> tiles = new java.util.ArrayList<Tile>();
	
	public static java.util.List<Tile> getTiles() {
		return tiles;
	}
	
	public Board() {
		Cards.initialize();
		Players.initialize();
		
        
        for(int i = 0; i < DimensionX; i += TileDimension) { 
        	tiles.add(new Tile(Tile.TileType.SOLID, i, 0));
        	tiles.add(new Tile(Tile.TileType.SOLID, i, DimensionY-TileDimension));
        }
        
        for(int i = 0; i < DimensionY; i += TileDimension) {
        	tiles.add(new Tile(Tile.TileType.SOLID, 0, i));   
        	tiles.add(new Tile(Tile.TileType.SOLID, DimensionX-TileDimension, i)); 
        }  
        
        //Room 1
        tiles.add(new Tile(Tile.TileType.SOLID, 5*TileDimension, 4*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 6*TileDimension, 4*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 7*TileDimension, 4*TileDimension));        
        tiles.add(new Tile(Tile.TileType.SOLID, 5*TileDimension, 5*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 5*TileDimension, 6*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 7*TileDimension, 5*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 7*TileDimension, 6*TileDimension));
        tiles.add(new Tile(Tile.TileType.ROOM, 6*TileDimension, 5*TileDimension).setRoomId(1));
        tiles.add(new Tile(Tile.TileType.ROOM, 6*TileDimension, 6*TileDimension).setRoomId(1));       
        
        
        
       //Room 2
        tiles.add(new Tile(Tile.TileType.SOLID, 5*TileDimension, 12*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 6*TileDimension, 12*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 7*TileDimension, 12*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 5*TileDimension, 13*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 7*TileDimension, 13*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 5*TileDimension, 14*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 7*TileDimension, 14*TileDimension));
        tiles.add(new Tile(Tile.TileType.ROOM, 6*TileDimension, 13*TileDimension).setRoomId(2));
        tiles.add(new Tile(Tile.TileType.ROOM, 6*TileDimension, 14*TileDimension).setRoomId(2));  
      //Room 3
        tiles.add(new Tile(Tile.TileType.SOLID, 5*TileDimension, 20*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 6*TileDimension, 20*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 7*TileDimension, 20*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 5*TileDimension, 21*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 7*TileDimension, 21*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 5*TileDimension, 22*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 7*TileDimension, 22*TileDimension));
        tiles.add(new Tile(Tile.TileType.TELEPORT, 6*TileDimension, 21*TileDimension).setRoomId(3));
        tiles.add(new Tile(Tile.TileType.ROOM, 6*TileDimension, 22*TileDimension).setRoomId(3));  
        //Room 4
        tiles.add(new Tile(Tile.TileType.SOLID, 15*TileDimension, 4*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 16*TileDimension, 4*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 17*TileDimension, 4*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 15*TileDimension, 5*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 17*TileDimension, 5*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 15*TileDimension, 6*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 17*TileDimension, 6*TileDimension));
        tiles.add(new Tile(Tile.TileType.ROOM, 16*TileDimension, 5*TileDimension).setRoomId(4));
        tiles.add(new Tile(Tile.TileType.ROOM, 16*TileDimension, 6*TileDimension).setRoomId(4));  
        //Room 5
        tiles.add(new Tile(Tile.TileType.SOLID, 15*TileDimension, 12*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 16*TileDimension, 12*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 17*TileDimension, 12*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 15*TileDimension, 13*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 17*TileDimension, 13*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 15*TileDimension, 14*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 17*TileDimension, 14*TileDimension));
        tiles.add(new Tile(Tile.TileType.ROOM, 16*TileDimension, 13*TileDimension).setRoomId(5));
        tiles.add(new Tile(Tile.TileType.ROOM, 16*TileDimension, 14*TileDimension).setRoomId(5));  
        //Room 6
        tiles.add(new Tile(Tile.TileType.SOLID, 15*TileDimension, 20*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 16*TileDimension, 20*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 17*TileDimension, 20*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 15*TileDimension, 21*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 17*TileDimension, 21*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 15*TileDimension, 22*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 17*TileDimension, 22*TileDimension));
        tiles.add(new Tile(Tile.TileType.ROOM, 16*TileDimension, 21*TileDimension).setRoomId(6));
        tiles.add(new Tile(Tile.TileType.ROOM, 16*TileDimension, 22*TileDimension).setRoomId(6));  
		//Room 7
        tiles.add(new Tile(Tile.TileType.SOLID, 25*TileDimension, 4*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 26*TileDimension, 4*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 27*TileDimension, 4*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 25*TileDimension, 5*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 27*TileDimension, 5*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 25*TileDimension, 6*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 27*TileDimension, 6*TileDimension));
        tiles.add(new Tile(Tile.TileType.TELEPORT, 26*TileDimension, 5*TileDimension).setRoomId(7));
        tiles.add(new Tile(Tile.TileType.ROOM, 26*TileDimension, 6*TileDimension).setRoomId(7));  
		//Room 8
		tiles.add(new Tile(Tile.TileType.SOLID, 25*TileDimension, 12*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 26*TileDimension, 12*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 27*TileDimension, 12*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 25*TileDimension, 13*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 27*TileDimension, 13*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 25*TileDimension, 14*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 27*TileDimension, 14*TileDimension));
        tiles.add(new Tile(Tile.TileType.ROOM, 26*TileDimension, 13*TileDimension).setRoomId(8));
        tiles.add(new Tile(Tile.TileType.ROOM, 26*TileDimension, 14*TileDimension).setRoomId(8));  
        //Room 9
        tiles.add(new Tile(Tile.TileType.SOLID, 25*TileDimension, 20*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 26*TileDimension, 20*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 27*TileDimension, 20*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 25*TileDimension, 21*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 27*TileDimension, 21*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 25*TileDimension, 22*TileDimension));
        tiles.add(new Tile(Tile.TileType.SOLID, 27*TileDimension, 22*TileDimension));
        tiles.add(new Tile(Tile.TileType.ROOM, 26*TileDimension, 21*TileDimension).setRoomId(9));
        tiles.add(new Tile(Tile.TileType.ROOM, 26*TileDimension, 22*TileDimension).setRoomId(9));  
	}
	@Override
    public void addNotify() {
        super.addNotify();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);        

        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, DimensionX, DimensionY);   
        
       
        for(Tile t : tiles) {
        	g2d.setColor(t.getColor());
        	g2d.fillRect(t.getX(), t.getY(), TileDimension, TileDimension);
        	if(t.getTileType().equals(TileType.ROOM)) {
        		g.setColor(Color.BLACK);
        		g.drawString(""+t.getId(), t.getX()+2, t.getY()+13);
        	}
        }  
        
        for(Player p : Players.getPlayers()) {
        	g.setColor(Color.GREEN);
        	g2d.fillRect(p.getX(), p.getY(), TileDimension, TileDimension);     
        	g.setColor(Color.BLACK);
    		g.drawString(""+p.getName().charAt(0), p.getX()+2, p.getY()+13);
        }   
        
        Toolkit.getDefaultToolkit().sync();
        g.dispose();
    }
}
