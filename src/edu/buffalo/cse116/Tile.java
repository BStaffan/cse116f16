package edu.buffalo.cse116;

import java.awt.Color;

/*
 * @Author Alek
 */
public class Tile {
	
	public enum TileType {NON_SOLID, SOLID, ROOM, TELEPORT}
	
	//Used to separate rooms
	TileType type = TileType.NON_SOLID;
	int id = 0;
	int x, y;
	Color color;
	
	Tile(TileType type, int x, int y) {
		this.type = type;
		this.x = x;
		this.y = y;
		switch(type) {
		case NON_SOLID:
			color = Color.GRAY;
			break;
		case SOLID:
			color = Color.BLACK;
			break;
		case ROOM:
			color = Color.RED;
			break;
		case TELEPORT:
			color = Color.MAGENTA;
			break;
		}
	}
	
	public Tile setRoomId(int id) {
		this.id = id;
		return this;
	}
	
	public TileType getTileType() {
		return type;
	}
	
	public int getX() {
		return x;		
	}
	
	public int getY() {
		return y;
	}
	
	public int getId() {
		return id;
	}
	
	public Color getColor() {
		return color;
	}

}
