package edu.buffalo.cse116;
/*
 * @author Alek, Ben
 */
public class Card {
	
	public enum CardType {ROOM, SUSPECT, WEAPON}
	private CardType cardType;
	private String name;
	/**Constructor for Card type builds card with Card Type and name
	 *
	 * @param  CardType cardtype, String Name
	 * @return None
	 * @author Alek
	 */
	public Card(CardType cardType, String name) {
		this.cardType = cardType;
		this.name = name;
	}	
	
	public CardType getType() {
		return cardType;		
	}
	
	public String getName() {
		return name;
	}
	/**equals method to check if card is equal to other cards
	 *
	 * @param  none
	 * @return return true if card is equal to another card
	 * @author Ben, Professor Hertz!
	 */
	@Override
	public boolean equals(Object card){
		if (card instanceof Card) {
			Card oCard = (Card)card; 
		boolean retVal = false;
			if(name.equals(oCard.getName()))
			{
				retVal = true;
			}
		return retVal;
		} return false;
	}

}
