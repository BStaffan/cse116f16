package edu.buffalo.cse116;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
/*
 * @author Alek, Harrison, Ben, Qian Ying Chen
 */
public class Player extends Movement {
	
	String name;
	int locationX;
	int locationY;
	int moves;
	Boolean status;
	java.util.List<Card> cards = new java.util.ArrayList<Card>();
	
	Player()
	{
		name = null;
		locationX = 0;
		locationY = 0;
		cards = null;
		status = true;
	}
	
	Player(String name, java.util.List<Card> cards, int x, int y) {
		this.name = name;
		this.cards = cards;
		this.locationX = x;
		this.locationY = y;
		status = true;
	}
	
	public void setMoves(int moves) {
		this.moves = moves;
	}
	
	public int getMoves() {
		return moves;
	}
	
	public void useMove() {
		moves--;
	}
	
	public boolean hasMatchingAccusationCard(java.util.List<Card> accusation) {
		return !Collections.disjoint(cards, accusation);
	}	

	public void setCards(java.util.List<Card> cards) {
		this.cards = cards;
	}
	public boolean getStatus(){
		return status;
	}
	public String getName() {
		return name;
	}
	public java.util.List<Card> addToHand(Card card) {
		cards.add(card);
		return cards;
	}
	
	public java.util.List<Card> getHand() {
		return cards;
	}

	public void isOut(boolean status){
		this.status = false;
	}
	/**
	 * 
	 *
	 * @return players x location
	 * @author Ben
	 */
	public int getX()
	{
		return locationX;
	}
	/**
	 * 
	 * @param  getsthe ycord as the y location 
	 * @return y location on player
	 * @author Ben
	 */
	public int getY()
	{
		return locationY;
	}
	/**
	 * 
	 * @param int xcord
	 *  sets the xcord as the xlocation 
	 * @return void
	 * @author Ben
	 */
	public void setX(int xCord)
	{
			locationX += xCord;	
	}
	/**
	 * 
	 * @param int ycord
	 *  sets the ycord as the y location 
	 * @return void
	 * @author Ben
	 */
	public void setY(int yCord)
	{
			locationY += yCord;	
	}
	
	//*********************************************
	//this is the guess method
	//********************************************
	public boolean makeSuggestion(){
		String suspect=JOptionPane.showInputDialog("Enter suspect: ").toLowerCase();
		String room=JOptionPane.showInputDialog("Enter room: ").toLowerCase();
		String weapon=JOptionPane.showInputDialog("Enter weapon: ").toLowerCase();
		String name="";
		boolean found=false;
		List<Card> playerTotalCards=new ArrayList<>();
		for(Player p:Players.players)
			playerTotalCards.addAll(p.cards);
		//Player nextPlayer=Players.getNextPlayer();
		for(int i=0;i<playerTotalCards.size();i++){
			if(playerTotalCards.get(i).getName().toLowerCase().equals(suspect.toLowerCase())||playerTotalCards.get(i).getName().toLowerCase().equals(room.toLowerCase())||playerTotalCards.get(i).getName().toLowerCase().equals(weapon.toLowerCase())){
				for(Player pp:Players.players){
					if(pp.getHand().contains(playerTotalCards.get(i))){
						if (pp.getName() == Players.getCurrentPlayer().getName()){
						System.out.println("Suggested cards not found");
						found=true;
						} else{
						name=pp.getName();
						System.out.println("The card: "+playerTotalCards.get(i).getName()+ " has been found in this players hand: "+name+ " ");
						found=true;
						}
					}
				}
				
			}
			}
		if(found==true)
			return true;
		else{
		System.out.print("Suggested cards not found");
		return false;
	}
	}


	/**
	 * 
	 * @param Direction-desired location to move
	 * @param  px-pixels being moved
	 * @return false or true depending on validity of move
	 * @author Harrison, Alek, Ben
	 */
		public boolean playerMove(String direction, int px)
		{
			
			
		
		int destinationX = getX();
		int destinationY = getY();
		
		switch(direction) {
		case "up":
			destinationY = destinationY-px;
			break;
		case "down":
			destinationY = destinationY+px;
			break;
		case "left":
			destinationX = destinationX - px;
			break;
		case "right":
			destinationX = destinationX + px;
			break;
		}
		
		for(Tile t : Board.tiles) {
			if(t.getX() == destinationX && t.getY() == destinationY) {
				if(t.getTileType().equals(Tile.TileType.SOLID)){
					return false;
				}
				else if(!t.getTileType().equals(Tile.TileType.ROOM)){
					return false;
				}
				
			}
			if( destinationX == 96 && destinationY == 336)
			{
				locationX=416; 
				locationY=80; 
			}
			else if(getX()==416 && getY()==80){
				locationX=96;
				locationY=336;
			}
		}
		locationX=destinationX;
		locationY=destinationY;
		return true;
		
			
			/*boolean retVal = false;
			
	
			
				switch(direction)
				{
					case "up":	
						for(Tile t:Board.tiles){
							if(t.getY()==this.getY()-16){
									
										if(t.getTileType().equals(Tile.TileType.SOLID)){
											return false;
										}
									else{
									setY(moveNorth(px));
									System.out.println(this.getX()+ " "+this.getY());
									retVal = true;
									break;
									}
								}
						}
								return retVal;
								
					case "down":	
						for(Tile t:Board.tiles){
							if(t.getY()==this.getY()+16){
								if(t.getTileType().equals(Tile.TileType.SOLID)){
									return false;
								}
							}
							setY(moveSouth(px));
							System.out.println(this.getX()+ " "+this.getY());
							retVal = true;
							break;
						}
			
						return retVal;
									
					case "left": 	
						for(Tile t:Board.tiles){
							if(t.getX()==this.getX()-16){
								if(t.getTileType().equals(Tile.TileType.SOLID)){
									return false;
								}
							}
							setX(moveWest(px));
							System.out.println(this.getX()+ " "+this.getY());
							retVal = true;
							break;
						}
			
						return retVal;
									
					case "right":	
						for(Tile t:Board.tiles){
						if(t.getX()==this.getX()+16){
							if(t.getTileType().equals(Tile.TileType.SOLID)){
								return false;
							}
						}
						setX(moveEast(px));
						System.out.println(this.getX()+ " "+this.getY());
						retVal = true;
						break;
					}
		
					return retVal;
				}
				return retVal;
			*/
		}

		//********************************************
		//use for if we do tiles. can keep count and movement in player rather than board.
		public void openDoor()
		{
			if(getX()==5 && getY()==3)
			{
				locationX=3; 
				locationY=3; 
			}
		}

		/*
		public void passageMove(boolean yes)
		{
			if(true)
			{
				if(getX()==96 && getY()==96)
				{
					locationX=416; 
					locationY=352; 
				}
				else if(getX()==416 && getY()==96){
					locationX=96;
					locationY=352;
				}
				else if(getX()==416 && getY()==352){
					locationX=96;
					locationY=96;
				}
				else{
					locationX=416;
					locationY=96;
				}
			}
		}
		
		*/
		//*********************************************
	
}
