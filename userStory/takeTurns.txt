Author: Alek

1. Explain how to verify your program defines all 6 players.

The Players class instantiates new Player objects by going iterating through a simple string array. Prior to Players being
initialized however, the Cards class must be initialized because the constructor for Player takes a collection of cards called "Hand".

For verification, there is a simple console print which displays all the players along with their cards. Additionally, the interace displays
the current player and their hand.


2. Explain how to see that turns rotate between all 6 players in the correct order.
Player objects are created and stored in the Players class when the Board is created. The Players class keeps track of the
current player, starting with index 0. When we want the next player, we get the next index. In the case of no more players in the collection,
we loop back to index 0.

TheGame class contains our main method and also extends JFrame. In the JFrame we have buttons with action listeners. When the "Roll" button is clicked,
"moves" are added to a player. From this point a player cannot re-roll, however the movement controls are now enabled. Each time a movement button
is clicked, "moves" are subtracted from a player until it reaches 0. Once "moves" for the current player are 0, we disable movement buttons, enable
the roll button, and set the next player as mentioned above.